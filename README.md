Сборка проекта:
```
python setup.py bdist_wheel
```

Запуск тестов:
```
pip install -r requirements/development.txt
pytest tests --cov package
```

Запуск образа в Docker:
```
docker run -itd -p 8085:8085 registry.gitlab.com/adrnin/devops-test:latest
```
http://192.168.35.146:8085/api/service

```
service	"Test Package"
version	"1.0"
build	"2203121903"
```
